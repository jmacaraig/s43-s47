//ROUTES

const express = require("express");

const router = express.Router();

const userController = require("../controllers/user");

const courseController = require("../controllers/course");

const auth = require ("../auth");

const {verify, verifyAdmin} = auth;


router.post("/", verify, verifyAdmin, courseController.addCourse);

router.get("/all", courseController.getAllCourses);

//getAllActiveCourses

router.get("/", courseController.getAllActiveCourses);

router.get("/:courseId", courseController.getCourse);

router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);

module.exports = router;
