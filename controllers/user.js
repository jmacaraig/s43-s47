//controller 

const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqbody) => {

	return User.find({email: reqbody.email})

	.then(result => {

		if (result.length > 0){
			return true;
		}
		else {
			return false;
		}

	})

}

module.exports.registerUser = (reqbody) => {

	let newUser = new User ({

		firstName: reqbody.firstName,
		lastName: reqbody.lastName,
		email: reqbody.email,
		password:bcrypt.hashSync(reqbody.password,10),
		mobileNo:reqbody.mobileNo	
	})

	return newUser.save().then((user,error)=>{

		if (error){
			return true;
		}
		else {
			return false;
		}

	
	})

	.catch(err=>err)

}

module.exports.loginUser = (req,res)=>{

	console.log(req.body)
	return User.findOne({email: req.body.email})
	.then(result=>{

		if (result===null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)
		
		if (isPasswordCorrect){

			return res.send({access: auth.createAccessToken(result)})

		}else {
			return res.send(false);
		}
	}
	})
	.catch(err => res.send(err))
} 


module.exports.getProfile = (req,res)=>{
	
	console.log(req.body)
	return User.findById(req.user.id)
	.then(result=>{

		result.password = "";
		return res.send(result);
	
	})
	

} 


// module.exports.getProfile = (user)=>{

// 	return User.findById({_id: user.id})
// 	.then(result=>{

// 		result.password = "";
// 		return result
	
// 	})
	
// } 


module.exports.enroll = async (req,res) => {

	console.log(req.user.id);
	console.log(req.body.courseId);

	if (req.user.isAdmin){
		return res.send("Action Forbidden");
	}

	let isUserUpdated = await User.findById(req.user.id).then(user =>{

		let newEnrollment = {

			courseId: req.body.courseId

		}

		user.enrollments.push(newEnrollment);

		return user.save().then(user=> true).catch(err => err.message);
	})

	if (isUserUpdated !== true){

		return res.send({message: isUserUpdated});

	}

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course =>{

		let newEnrollee = {

			userId: req.user.id

		}

		course.enrollees.push(newEnrollee);

		return course.save().then(course => true).catch(err => err.message);

})
	if (isCourseUpdated !== true){

		return res.send({message: isCourseUpdated});

	}

	if (isUserUpdated && isCourseUpdated){

		return res.send({message: "Enrolled Successfully."})

	}

}