//CONTROLLER

const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth")


module.exports.addCourse = (req,res) => {
console.log(req.body)
	let newCourse = new Course ({

		name : req.body.name,
		description : req.body.description,
		price : req.body.price

	})

	return newCourse.save().then((course,error)=>{

		if (course){
			res.send(true)
			return
		}
		else {
			res.send(false)
			return
		}

	
	})

	.catch(err=>err)
}

module.exports.getAllCourses = (req,res)=>{

	return Course.find({}).then(result=>{

		return res.send(result)

	})

}

//getAllActiveCourses

module.exports.getAllActiveCourses = (req,res)=>{

	return Course.find({

		isActive: true

	}).then(result=>{

		return res.send(result)

	})

}


module.exports.getCourse = (req,res)=>{

	return Course.findById(req.params.courseId).then(result=>{

		return res.send(result)

	})

}


module.exports.updateCourse = (req,res)=>{

	let updatedCourse = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price
	}

	return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course,error)=>{

		if (error){
			return res.send(false);
		}else{
			return res.send(true);
		}

	})

	}


module.exports.archiveCourse = (req,res)=>{

	let archive = {

		isActive: req.body.isActive

	}

	return Course.findByIdAndUpdate(req.params.courseId, archive).then((course,error)=>{

		if (error){
			return res.send(false);
		}else{
			return res.send(true);
		}

	})

}

module.exports.activateCourse = (req,res)=>{

	let activate = {

		isActive: req.body.isActive

	}

	return Course.findByIdAndUpdate(req.params.courseId, activate).then((course,error)=>{

		if (error){
			return res.send(false);
		}else{
			return res.send(true);
		}

	})

}
