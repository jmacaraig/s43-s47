//Create a simple Express JS application

//Dependencies and modules

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");

//Environment Setup
const port = 4000;

//Server Setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
//Allows backend application to be available to frontend
app.use(cors());


mongoose.connect("mongodb+srv://johnedisonmacaraig:9KJbQmt7MrOHlsmA@batch-297.zitwj9u.mongodb.net/s43-s47?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});


let db = mongoose.connection;

//console.error.bind(console) print errors in browser console and terminal
db.on("error", console.error.bind(console, "Connection error."));

//if the connection is successful output in console
db.once("open", () => console.log("Now Connected to MongoDB Atlas."));

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);







//Server Gateway Response
if (require.main === module){

	app.listen(process.env.PORT || port,()=>{console.log(`API is now online on port ${process.env.PORT || port}.`)

})

}

module.exports = {app, mongoose};